<?php
/**
 * Connecteur et autre outils pour les bases de donnees.
 */
function connect()
{
    $host = 'localhost'; // localhost
    $db   = 'phortnot_mvade'; // nom de la base de donnée
    $user = 'root'; // utilisateur
    $pass = ''; //mot de passe
    $charset = 'utf8mb4'; // encodage
    $options = [
        PDO::ATTR_ERRMODE => PDO::ERRMODE_EXCEPTION,
        PDO::ATTR_DEFAULT_FETCH_MODE => PDO::FETCH_ASSOC, // Pour le moment fetch assoc cest parfait. 
        PDO::ATTR_EMULATE_PREPARES   => false,
    ];
    $dsn = "mysql:host=$host;dbname=$db;charset=$charset";

    try {
        return new PDO($dsn, $user, $pass, $options);
    } catch (PDOException $e) {
        echo $e->getMessage();
    }
}
